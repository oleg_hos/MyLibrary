package sample;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.helpers.ConectorDB;
import sample.helpers.ControllerResolver;

import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class ControllerAddReader implements Initializable {
    @FXML
    private TextField firstName;
    @FXML
    private TextField lastName;
    @FXML
    private TextField telephone;
    @FXML
    private Button addReaderBut;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void addReader() throws SQLException {
        String str = "INSERT INTO reader (firstName, lastName ,telephone) VALUES(?, ?, ?)";

        try (PreparedStatement pr = ConectorDB.getInstance().prepareStatement(str)) {
            pr.setString(1, firstName.getText());
            pr.setString(2, lastName.getText());
            pr.setString(3, telephone.getText());

            if (firstName.getText().isEmpty() || lastName.getText().isEmpty() || telephone.getText().isEmpty()) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("УВАГА");
                alert.setHeaderText("Не заповнено всі поля");
                alert.setContentText("Будь-ласка заповніть всі поля");

                alert.showAndWait();
            } else {
                pr.executeUpdate();
                ((Stage)addReaderBut.getScene().getWindow()).close();


                ControllerManagmentReaders c = ControllerResolver.getInstance().getController(ControllerManagmentReaders.class);
                c.fillReaderDataTable();


            }
        }
    }
}

