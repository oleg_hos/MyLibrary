package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import sample.helpers.Autorized;
import sample.helpers.BookDBResolver;
import sample.helpers.ConectorDB;
import sample.helpers.ControllerResolver;
import sample.model.Book;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    // private ObservableList<Book> usersData = FXCollections.observableArrayList();

    @FXML
    Button LoginButton;
    @FXML
    Button ManagerReaders;
    @FXML
    private TableView<Book> tableBooks;
    @FXML
    private TableView<Book> tableBooksFilter;
    @FXML
    private TableColumn<Book, Integer> idColumn;
    @FXML
    private TableColumn<Book, Integer> idColumnFilter;
    @FXML
    private TableColumn<Book, String> nameBookCol;
    @FXML
    private TableColumn<Book, String> nameBookColFilter;
    @FXML
    private TableColumn<Book, String> nameAuthorCol;
    @FXML
    private TableColumn<Book, String> nameAuthorColFilter;
    @FXML
    private TableColumn<Book, String> namePublisherCol;
    @FXML
    private TableColumn<Book, String> namePublisherColFilter;
    @FXML
    private TableColumn<Book, Integer> quantityCol;
    @FXML
    private TextField filterField;
    @FXML
    private TextField id;
    @FXML
    private TextField Id;
    @FXML
    private TextField nameBook5;
    @FXML
    private TextField nameAuthor;
    @FXML
    private TextField quantity;
    @FXML
    private TextField namePublisher;
    @FXML
    private AnchorPane LoginAnchorPane;

    private ObservableList<Book> data;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        idColumn.setCellValueFactory(new PropertyValueFactory<Book, Integer>("id"));
        nameBookCol.setCellValueFactory(new PropertyValueFactory<Book, String>("nameBook"));
        nameAuthorCol.setCellValueFactory(new PropertyValueFactory<Book, String>("nameAuthor"));
        namePublisherCol.setCellValueFactory(new PropertyValueFactory<Book, String>("namePublisher"));
        quantityCol.setCellValueFactory(new PropertyValueFactory<Book, Integer>("quantity"));


        idColumnFilter.setCellValueFactory(new PropertyValueFactory<Book, Integer>("id"));
        nameBookColFilter.setCellValueFactory(new PropertyValueFactory<Book, String>("nameBook"));
        nameAuthorColFilter.setCellValueFactory(new PropertyValueFactory<Book, String>("nameAuthor"));
        namePublisherColFilter.setCellValueFactory(new PropertyValueFactory<Book, String>("namePublisher"));

        data = FXCollections.observableArrayList();
        ControllerResolver.getInstance().saveController(this);
        fillDataTable();
    }

    public void fillDataTable() {
        data.clear();

        Statement statement = null;
        try {
            statement = ConectorDB.getInstance().createStatement();

            try {
                String selectFromDB = "SELECT id,nameBook,nameAuthor,namePublisher,quantity from book";
                ResultSet rs = statement.executeQuery(selectFromDB);


                while (rs.next()) {
                    int id = rs.getInt("id");
                    String nameBook = rs.getString("nameBook");
                    String nameAuthor = rs.getString("nameAuthor");
                    String namePublisher = rs.getString("namePublisher");
                    int quantity = rs.getInt("quantity");

                    data.add(new Book(id, nameAuthor, nameBook, namePublisher, quantity));
                }
                tableBooks.setItems(data);
            } catch (Exception e) {

                e.printStackTrace();
                System.out.println("Error on Building Data");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String insertedBook() {
        return filterField.getText();
    }


    @FXML
    public void serchBookByName() {
        ObservableList<Book> dataSearch = BookDBResolver.getInstance().getBookByName(insertedBook());
        filterField.setText("");
        tableBooksFilter.setItems(dataSearch);
    }



    @FXML
    public void stLoginButton() {

        try {
            Stage stage = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("views/login.fxml"));
            stage.setTitle("Авторизація");
            stage.setResizable(false);
            stage.setScene(new Scene(root));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
        LoginButton.setVisible(false);
    }


    public void addBook() {
        if (Autorized.res) {
            LoginButton.setVisible(false);
            String str = "INSERT INTO book (nameBook, nameAuthor ,namePublisher, quantity) VALUES(?, ?, ?, ?)";
            String i = quantity.getText();
            if (!i.isEmpty()) {

                try (PreparedStatement pr = ConectorDB.getInstance().prepareStatement(str)) {
                    pr.setString(1, nameBook5.getText());
                    pr.setString(2, nameAuthor.getText());
                    pr.setString(3, namePublisher.getText());
                    pr.setInt(4, Integer.parseInt(quantity.getText()));

                    if (nameBook5.getText() == null || nameBook5.getText().isEmpty()) {
                        Alert alert = new Alert(Alert.AlertType.WARNING);
                        alert.setTitle("УВАГА");
                        alert.setHeaderText("Назва не може бути порожньою");
                        alert.setContentText("Будь-ласка введіть назву книги");
                        alert.showAndWait();
                    } else {
                        if (Integer.parseInt(quantity.getText()) > 0) {
                            pr.executeUpdate();
                            nameBook5.setText(" ");
                            nameAuthor.setText(" ");
                            namePublisher.setText(" ");
                            quantity.setText(" ");

                            Alert alert = new Alert(Alert.AlertType.INFORMATION);
                            alert.setTitle("Виконано");
                            alert.setHeaderText("Додавання книги");
                            String s = "Книгу успішно додано";
                            alert.setContentText(s);
                            alert.show();

                        } else {
                            Alert alert = new Alert(Alert.AlertType.WARNING);
                            alert.setTitle("УВАГА");
                            alert.setHeaderText("Кількість не може бути відємною");
                            alert.setContentText("Будь-ласка перевірте кількість книг");
                            alert.showAndWait();
                        }
                        fillDataTable();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("УВАГА");
                    alert.setHeaderText("Кількість не може бути порожньою");
                    alert.setContentText("Будь-ласка введіть кількість книг");
                    alert.showAndWait();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            notAuthorized();
        }
    }

    public void deleteBook() {
        if (Autorized.res) {
            LoginButton.setVisible(false);
            if (Id.getText().isEmpty()) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("УВАГА");
                alert.setHeaderText("Книгу не видалено");
                alert.setContentText("Будь-ласка введіть id книги");
                alert.showAndWait();
            } else {
                try (PreparedStatement pr = ConectorDB.getInstance().prepareStatement("DELETE FROM book WHERE ID=?")) {
                    pr.setInt(1, Integer.parseInt(Id.getText()));
                    pr.executeUpdate();
                    id.setText("");
                    fillDataTable();
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Виконано");
                    alert.setHeaderText("Видалення книги");
                    String s = "Книгу успішно видалено";
                    alert.setContentText(s);
                    alert.show();

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } else {
            notAuthorized();
        }
    }

    @FXML
    public void updateBook() {
        LoginButton.setVisible(false);

        if (Autorized.res) {
            Book book = null;

            if (Id.getText().isEmpty()) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("УВАГА");
                alert.setHeaderText("Книгу не зінено");
                alert.setContentText("Будь-ласка введіть id книги");
                alert.showAndWait();
            } else {

                try (PreparedStatement pr = ConectorDB.getInstance().prepareStatement("select * FROM book WHERE ID=?")) {
                    pr.setInt(1, Integer.parseInt(Id.getText()));
                    ResultSet rs = pr.executeQuery();
                    if (rs.next()) {
                        book = BookDBResolver.getInstance().createBookFromResultSet(rs);
                        if (book.getQuantity() + Integer.parseInt(quantity.getText()) > 0) {
                            book.setQuantity(book.getQuantity() + Integer.parseInt(quantity.getText()));
                        } else {
                            Alert alert = new Alert(Alert.AlertType.WARNING);
                            alert.setTitle("Увага");
                            alert.setHeaderText("Поновлення нформації про  книги");
                            String s = "Кількість не може бути відємною";
                            alert.setContentText(s);
                            alert.show();
                            quantity.setText(" ");
                            Id.setText("");
                        }
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (book != null) {
                try (PreparedStatement pr = ConectorDB.getInstance().prepareStatement("UPDATE `booklibrary`.`book` SET `quantity` = ? WHERE `book`.`id` = ?")) {
                    pr.setInt(1, book.getQuantity());
                    pr.setInt(2, Integer.parseInt(Id.getText()));
                    pr.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            fillDataTable();
            quantity.setText(" ");
            Id.setText("");
        } else {
            notAuthorized();
        }
    }


    @FXML
    public void notAuthorized() {

        try {
            Stage stage = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("views/notLoggin.fxml"));
            stage.setTitle("Помилка доступу");
            stage.setResizable(false);
            stage.setScene(new Scene(root));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    public void minusCol() {
//
//        try {
//            Stage stage = new Stage();
//            Parent root = FXMLLoader.load(getClass().getResource("views/MinusCol.fxml"));
//            stage.setTitle("Помилка кількості");
//            stage.setResizable(false);
//            stage.setScene(new Scene(root));
//            stage.initModality(Modality.APPLICATION_MODAL);
//            stage.show();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

//    public void nullName() {
//        try {
//            Stage stage = new Stage();
//            Parent root = FXMLLoader.load(getClass().getResource("views/nullName.fxml"));
//            stage.setTitle("Помилка.Порожня назва книги");
//            stage.setResizable(false);
//            stage.setScene(new Scene(root));
//            stage.initModality(Modality.APPLICATION_MODAL);
//            stage.show();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    public void managerReaders() {
        LoginButton.setVisible(false);
        if (Autorized.res) {
            try {
                Stage stage = new Stage();
                Parent root = FXMLLoader.load(getClass().getResource("views/managmentReaders.fxml"));
                stage.setTitle("Управління читачами");
                stage.setResizable(false);
                stage.setScene(new Scene(root));
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            notAuthorized();
        }
    }

}




















