package sample;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.helpers.Autorized;
import sample.helpers.Md5hashing;

import java.net.URL;
import java.util.ResourceBundle;

public class ControllerLogin implements Initializable {
    @FXML
    Button authorizedButton;
    @FXML
    TextField loginForm1;
//    @FXML
//    Button LoginButton;
    @FXML
    PasswordField passField;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public  void avtorization() {

        String login = loginForm1.getText();
        String p = Md5hashing.md5Custom(passField.getText());
        if ((login.equals("Admin")) && (p.equals("d8578edf8458ce06fbc5bb76a58c5ca4"))) {
            ((Stage)loginForm1.getScene().getWindow()).close();
            Autorized.autorized(1);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Інформація");
            alert.setHeaderText("Ви успішно увійшли до системи ");
            String s ="Ви авторизовані в системі як : Admin";
            alert.setContentText(s);
            alert.show();

        }
        else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("УВАГА");
            alert.setHeaderText("НЕВІРНО ВВЕДЕНО ЛОГІН АБО ПАРОЛЬ ");
            String s ="Перевірте логін та пароль";
            alert.setContentText(s);
            alert.show();

            ((Stage)loginForm1.getScene().getWindow()).close();
            Autorized.autorized(0);        }
    }
}
