package sample;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import sample.helpers.BookDBResolver;
import sample.helpers.ConectorDB;
import sample.helpers.ControllerResolver;
import sample.model.Book;
import sample.model.InRent;
import sample.model.Reader;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class ControllerManagmentReaders implements Initializable {
    @FXML
    private int id;
    @FXML
    private TableView<Reader> tableReaders;
    @FXML
    private TableView<InRent> inRenrTable;
    @FXML
    private TableColumn<Reader, String> firstNameCol;
    @FXML
    private TableColumn<Reader, String> telephoneCol;
    @FXML
    private Label firstNameLabel;
    @FXML
    private Label lastNameLabel;
    @FXML
    private Label telephoneLabel;
    @FXML
    private TableColumn<InRent, Integer> id2;
    @FXML
    private TableColumn<InRent, Integer> idColRentReader;
    @FXML
    private TableColumn<InRent, Integer> idColRentBook;
    @FXML
    private TableColumn<InRent, String> nameColRentBook;
    @FXML
    private TextField filterId;
    @FXML
    private Button IdBook;

    private ObservableList<Reader> readerData;
    private ObservableList<InRent> inRentsData;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        firstNameCol.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        telephoneCol.setCellValueFactory(new PropertyValueFactory<>("telephone"));
        readerData = FXCollections.observableArrayList();
        fillReaderDataTable();
        // Ініціалізація таблиці.
        // Очищаємо данні про читача
        showReaderDetails(null);

        // Добавляємо слухача вибору користувача
        tableReaders.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showReaderDetails(newValue));


        ControllerResolver.getInstance().saveController(this); //registering this controller

        id2.setCellValueFactory(new PropertyValueFactory<InRent, Integer>("id"));
        idColRentBook.setCellValueFactory(new PropertyValueFactory<InRent, Integer>("idBook"));
        idColRentReader.setCellValueFactory(new PropertyValueFactory<InRent, Integer>("idReader"));
        nameColRentBook.setCellValueFactory(new PropertyValueFactory<InRent, String>("nameBookRent"));
    }


    public void fillReaderDataTable() {
        readerData.clear();

        Statement statement = null;
        try {
            statement = ConectorDB.getInstance().createStatement();

            try {
                String selectFromDB = "SELECT * from reader";
                ResultSet rs = statement.executeQuery(selectFromDB);


                while (rs.next()) {
                    int id = rs.getInt("id");
                    String firstName = rs.getString("firstName");
                    String lastName = rs.getString("lastName");
                    String telephone = rs.getString("telephone");

                    readerData.add(new Reader(id, firstName, lastName, telephone));
                }
                tableReaders.setItems(readerData);
            } catch (Exception e) {

                e.printStackTrace();
          //      System.out.println("Error on Building Data");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showReaderDetails(Reader reader) {
        if (reader != null) {
            // Заповнюємо мітка
            firstNameLabel.setText(reader.getFirstName());
            lastNameLabel.setText(reader.getLastName());
            telephoneLabel.setText(reader.getTelephone());
            //  retI(reader.getId());


            ObservableList<InRent> dataInRent;
            dataInRent = FXCollections.observableArrayList();

            try {
                PreparedStatement ps = ConectorDB.getInstance().prepareStatement("SELECT * from inrent where idReader =?");
                int idReader = reader.getId();
                ps.setInt(1, idReader);

                ResultSet rs = ps.executeQuery();

                while (rs.next()) {
                    InRent inRent = createInRentFromResultSet(rs);
                    Book book = BookDBResolver.getInstance().getBookById(inRent.getIdBook());
                    inRent.setNameBookRent(book.getNameBook());
                    dataInRent.add(inRent);
                }

                inRenrTable.setItems(dataInRent);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else{
            // Якщо нуль то очищаємо мітки// vj;r
            firstNameLabel.setText("");
            lastNameLabel.setText("");
            telephoneLabel.setText("");
        }

    }

    public int selectedBook() {


        return Integer.parseInt(filterId.getText());
    }

    @FXML
public void returnInRent(){
     //   System.out.println(selectedBook());
        String str = "DELETE  FROM inrent WHERE idBook = ? and idReader = ?";
        Book book = BookDBResolver.getInstance().getBookById(selectedBook());
        Reader reader = tableReaders.getSelectionModel().getSelectedItem();
        try (PreparedStatement pr = ConectorDB.getInstance().prepareStatement(str)) {
            pr.setInt(1, Integer.parseInt(filterId.getText()));
            pr.setInt(2, reader.getId());
     //       Integer idBook = Integer.parseInt(filterId.getText());
    //        System.out.println(idBook);
            pr.executeUpdate();
            updateView();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try (PreparedStatement pr1 = ConectorDB.getInstance().prepareStatement("UPDATE `book` SET `quantity` = ? WHERE `book`.`id` = ?")) {


            pr1.setInt(1, book.getQuantity() + 1);
            pr1.setInt(2, Integer.parseInt(filterId.getText()));
            pr1.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        Controller c = ControllerResolver.getInstance().getController(Controller.class);
        c.fillDataTable();

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Виконано");
        alert.setHeaderText("Книгу повернуто до бібліотеки");
        String s = ("Книгу успішно повернуто");
        alert.setContentText(s);
        alert.show();
        filterId.setText("");
    }

    @FXML
    public void idBook() {
        Book book = BookDBResolver.getInstance().getBookById(selectedBook());
        if (book.getQuantity()>=1) {
 //       System.out.println(filterId.getText());
        String str = "INSERT INTO inrent (idBook, idReader ,nameBookRent) VALUES(?, ?, ?)";


        Reader reader = tableReaders.getSelectionModel().getSelectedItem();

            try (PreparedStatement pr = ConectorDB.getInstance().prepareStatement(str)) {
                pr.setInt(1, Integer.parseInt(filterId.getText()));
                pr.setInt(2, reader.getId());
                pr.setString(3, book.getNameBook());

                pr.executeUpdate();


            } catch (SQLException e) {
                e.printStackTrace();
            }
            updateView();

            try (PreparedStatement pr1 = ConectorDB.getInstance().prepareStatement("UPDATE `book` SET `quantity` = ? WHERE `book`.`id` = ?")) {


                pr1.setInt(1, book.getQuantity() - 1);
                pr1.setInt(2, Integer.parseInt(filterId.getText()));
                pr1.executeUpdate();

            } catch (SQLException e) {
                e.printStackTrace();
            }

            Controller c = ControllerResolver.getInstance().getController(Controller.class);
            c.fillDataTable();

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Виконано");
            alert.setHeaderText("Книгу віддано читачу");
            String s = ("Книгу успішно віддано");
            alert.setContentText(s);
            alert.show();
            filterId.setText("");
        }
        else{
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Увага");
            alert.setHeaderText("Книгу не віддано читачу");
            String s = ("Книг в бібіліотеці не достатньо");
            alert.setContentText(s);
            alert.show();
            filterId.setText("");
        }
        }


    private InRent createInRentFromResultSet(ResultSet rs) throws SQLException {
        InRent inRent = new InRent();
        inRent.setId(rs.getInt("id"));
        inRent.setIdBook(rs.getInt("idBook"));
        inRent.setIdReader(rs.getInt("idReader"));
        inRent.setNameBookRent(rs.getString("nameBookRent"));
        return inRent;
    }


    @FXML
    private void deleteReader() {

        Reader reader = tableReaders.getSelectionModel().getSelectedItem();
     //   System.out.println(reader.getId());
        if (reader != null) {

            try (PreparedStatement pr = ConectorDB.getInstance().prepareStatement("DELETE FROM reader WHERE id=?")) {

                pr.setInt(1, reader.getId());
                pr.executeUpdate();
                fillReaderDataTable();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            // Якщо не вибрано
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("УВАГА");
            alert.setHeaderText("Не вибрано користувача");
            alert.setContentText("Будь-ласка виберіть користувача з таблиці");

            alert.showAndWait();
        }
    }

    @FXML
    private void addReader() {
        try {
            Stage stage = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("views/addReader.fxml"));
            stage.setTitle("Додавання нового читача");

            stage.setResizable(false);
            stage.setScene(new Scene(root));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateView(){
        Reader reader = tableReaders.getSelectionModel().getSelectedItem();
        ObservableList<InRent> dataInRent;
        dataInRent = FXCollections.observableArrayList();
dataInRent.clear();
        try {
            PreparedStatement ps = ConectorDB.getInstance().prepareStatement("SELECT * from inrent where idReader =?");
            int idReader = reader.getId();
            ps.setInt(1, idReader);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                InRent inRent = createInRentFromResultSet(rs);
                Book book = BookDBResolver.getInstance().getBookById(inRent.getIdBook());
                inRent.setNameBookRent(book.getNameBook());
                dataInRent.add(inRent);
            }

            inRenrTable.setItems(dataInRent);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}