package sample.model;

public class InRent {
    private Integer id;
    private Integer idBook;
    private Integer idReader;
    private String nameBookRent;

    public InRent() {
    }

    public InRent(Integer id, Integer idBook, Integer idReader, String nameBookRent) {
        this.id = id;
        this.idBook = idBook;
        this.idReader = idReader;
        this.nameBookRent = nameBookRent;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdBook() {
        return idBook;
    }

    public void setIdBook(Integer idBook) {
        this.idBook = idBook;
    }

    public Integer getIdReader() {
        return idReader;
    }

    public void setIdReader(Integer idReader) {
        this.idReader = idReader;
    }

    public String getNameBookRent() {
        return nameBookRent;
    }

    public void setNameBookRent(String nameBookRent) {
        this.nameBookRent = nameBookRent;
    }
}


