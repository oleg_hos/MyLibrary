package sample.model;

public class Book {
    private Integer id;
    private String nameAuthor;
    private String nameBook;
    private String namePublisher;
    private Integer quantity;

    public Book(Integer id, String nameAuthor, String nameBook, String namePublisher, Integer quantity) {
        this.id = id;
        this.nameAuthor = nameAuthor;
        this.nameBook = nameBook;
        this.namePublisher = namePublisher;
        this.quantity = quantity;
    }

    public Book() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameAuthor() {
        return nameAuthor;
    }

    public void setNameAuthor(String nameAuthor) {
        this.nameAuthor = nameAuthor;
    }

    public String getNameBook() {
        return nameBook;
    }

    public void setNameBook(String nameBook) {
        this.nameBook = nameBook;
    }

    public String getNamePublisher() {
        return namePublisher;
    }

    public void setNamePublisher(String namePublisher) {
        this.namePublisher = namePublisher;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}


