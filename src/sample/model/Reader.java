package sample.model;


public class Reader {
    private Integer id;
    private String telephone;
    private String firstName;
    private String lastName;

    public Reader(Integer id,String firstName, String lastName,String telephone) {
        this.id = id;
        this.telephone = telephone;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}