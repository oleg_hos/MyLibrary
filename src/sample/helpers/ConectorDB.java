package sample.helpers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConectorDB {

    private Connection con;
    private static ConectorDB instance;

    private ConectorDB() throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://localhost:3306/booklibrary?characterEncoding=UTF-8";
            con = DriverManager.getConnection(url, "root", "root");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static synchronized Connection getInstance() {
        if (instance == null) {
            try {
                instance = new ConectorDB();
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
         //   System.out.println("Connection established");
        }
        return instance.con;
    }
}