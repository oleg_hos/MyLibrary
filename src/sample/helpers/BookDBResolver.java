package sample.helpers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sample.model.Book;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
public class BookDBResolver {
    private static BookDBResolver instance;

    public static BookDBResolver getInstance() {
        if (instance == null) {
            instance = new BookDBResolver();
        }
        return instance;
    }

    public ObservableList<Book> getBookByName(String name) {
        ObservableList<Book> result = FXCollections.observableArrayList();

        try {
            PreparedStatement ps = ConectorDB.getInstance().prepareStatement("SELECT * from book where nameBook =?");
            ps.setString(1, name);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Book book = createBookFromResultSet(rs);

                if (book.getQuantity() <= 0) {
                    System.out.println("No book avaliable");
                } else {
                    result.add(book);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Book getBookById(int id) {
        Book result = null;

        try {
            PreparedStatement ps = ConectorDB.getInstance().prepareStatement("SELECT * from book where id =?");
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                result = createBookFromResultSet(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Book createBookFromResultSet(ResultSet rs) throws SQLException {
        Book book = new Book();
        book.setId(rs.getInt("id"));
        book.setNameBook(rs.getString("nameBook"));
        book.setNameAuthor(rs.getString("nameAuthor"));
        book.setNamePublisher(rs.getString("namePublisher"));
        book.setQuantity(rs.getInt("quantity"));
        return book;
    }

}

