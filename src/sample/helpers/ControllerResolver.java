package sample.helpers;

import java.util.HashMap;
import java.util.Map;

public class ControllerResolver {

    private Map<Class,Object> controllerHolder = new HashMap<>();

    private ControllerResolver() {
        //just hiding constructor
    }

    public static ControllerResolver getInstance() {
        return Holder.instance;
    }

    private static class Holder {
        public static final ControllerResolver instance = new ControllerResolver();
    }

    public<T> void saveController(T controller){
        controllerHolder.put(controller.getClass(),controller);
    }

    public<T> T getController(Class<T> type){
        return (T) controllerHolder.get(type);
    }
}
