package sample;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ControllerNotLoggin implements Initializable {

    @FXML
    Button AvtorizedFormNotLoggin;
    @FXML
    Button EscFromNotLoggin;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void avtorizedFormNotLoggin(){

        ((Stage)AvtorizedFormNotLoggin.getScene().getWindow()).close();
        try {
            Stage stage = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("views/login.fxml"));
            stage.setTitle("Авторизація");
            stage.setResizable(false);
            stage.setScene(new Scene(root));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void escFromNotLoggin(){
        ((Stage)EscFromNotLoggin.getScene().getWindow()).close();
    }
}
